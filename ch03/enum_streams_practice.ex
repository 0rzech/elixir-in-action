defmodule EnumStream do

  def line_lengths!(path) do
    path
    |> line_stream!()
    |> Enum.map(&String.length/1)
  end

  def longest_line_length!(path) do
    path
    |> line_stream!()
    |> Stream.map(&String.length/1)
    |> Enum.max()
  end

  def longest_line!(path) do
    path
    |> line_stream!()
    |> Enum.max_by(&String.length/1)
  end

  def words_per_line!(path) do
    path
    |> line_stream!()
    |> Stream.map(&String.split/1)
    |> Enum.map(&length/1)
  end

  defp line_stream!(path) do
    path
    |> File.stream!()
    |> Stream.map(&String.replace(&1, "\n", ""))
  end
end
