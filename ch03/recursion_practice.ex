defmodule Recursion do
  def list_len([]), do: 0
  def list_len([_ | tail]), do: 1 + list_len(tail)

  def range(a, b) when is_integer(a) and is_integer(b) do
    cond do
      a == b -> [b]
      a < b -> [a | range(a + 1, b)]
      a > b -> [a | range(a - 1, b)]
    end
  end

  def positive([]), do: []
  def positive([head | tail]) when is_number(head) and head > 0, do: [head | positive(tail)]
  def positive([_ | tail]), do: positive(tail)
end
