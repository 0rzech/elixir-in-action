defmodule Recursion do
  def list_len(list) when is_list(list), do: list_len(list, 0)

  defp list_len([], len), do: len
  defp list_len([_ | tail], len), do: list_len(tail, len + 1)

  def range(a, b) when is_integer(a) and is_integer(b), do: range(a, b, [])

  defp range(a, b, result) when a == b, do: [a | result]
  defp range(a, b, result) when a < b, do: range(a, b - 1, [b | result])
  defp range(a, b, result) when a > b, do: range(a, b + 1, [b | result])

  def positive(list) when is_list(list), do: positive(list, [])

  defp positive([], result), do: Enum.reverse(result)
  defp positive([head | tail], result) when is_number(head) and head > 0, do: positive(tail, [head | result])
  defp positive([_ | tail], result), do: positive(tail, result)
end
