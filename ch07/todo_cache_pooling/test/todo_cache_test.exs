defmodule TodoCacheTest do
  use ExUnit.Case

  test "server_process" do
    {:ok, cache} = Todo.Cache.start()
    list_pid = Todo.Cache.server_process(cache, "list")

    assert list_pid != Todo.Cache.server_process(cache, "other list")
    assert list_pid == Todo.Cache.server_process(cache, "list")
  end

  test "to-do operations" do
    {:ok, cache} = Todo.Cache.start()
    list = Todo.Cache.server_process(cache, "list")
    Todo.Server.add_entry(list, %{date: ~D[2019-01-01], title: "Todo Title"})
    entries = Todo.Server.entries(list, ~D[2019-01-01])

    assert [%{date: ~D[2019-01-01], title: "Todo Title"}] = entries
  end
end
