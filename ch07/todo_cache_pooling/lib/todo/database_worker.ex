defmodule Todo.DatabaseWorker do
  use GenServer

  def start(db_folder), do: GenServer.start(__MODULE__, db_folder)
  def store(pid, key, data), do: GenServer.cast(pid, {:store, key, data})
  def get(pid, key), do: GenServer.call(pid, {:get, key})

  @impl GenServer
  def init(db_folder) do
    File.mkdir_p!(db_folder)
    {:ok, db_folder}
  end

  @impl GenServer
  def handle_cast({:store, key, data}, state) do
    file_name(state, key)
    |> File.write!(:erlang.term_to_binary(data))

    {:noreply, state}
  end

  @impl GenServer
  def handle_call({:get, key}, _, state) do
    data =
      case File.read(file_name(state, key)) do
        {:ok, contents} -> :erlang.binary_to_term(contents)
        _ -> nil
      end

    {:reply, data, state}
  end

  defp file_name(db_folder, key), do: Path.join(db_folder, to_string(key))
end
