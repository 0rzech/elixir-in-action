defmodule Todo.Database do
  use GenServer

  @db_folder "./persist"

  def start(), do: GenServer.start(__MODULE__, nil, name: __MODULE__)

  def store(key, data) do
    choose_worker(key) |> Todo.DatabaseWorker.store(key, data)
  end

  def get(key) do
    choose_worker(key) |> Todo.DatabaseWorker.get(key)
  end

  defp choose_worker(key), do: GenServer.call(__MODULE__, {:choose_worker, key})

  @impl GenServer
  def init(_) do
    File.mkdir_p!(@db_folder)

    pool =
      Enum.reduce(0..2, %{}, fn key, result ->
        {:ok, worker_pid} = Todo.DatabaseWorker.start(@db_folder)
        Map.put(result, key, worker_pid)
      end)

    {:ok, pool}
  end

  @impl GenServer
  def handle_call({:choose_worker, key}, _, state) do
    normalized_key = :erlang.phash2(key, 3)

    pid =
      case Map.fetch(state, normalized_key) do
        {:ok, pid} -> pid
        _ -> raise "Invalid normalized worker key: #{normalized_key}"
      end

    {:reply, pid, state}
  end
end
