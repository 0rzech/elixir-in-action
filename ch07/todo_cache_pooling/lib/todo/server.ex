defmodule Todo.Server do
  use GenServer

  def start(name, entries \\ []), do: GenServer.start(__MODULE__, {name, entries})

  def entries(todo_server, date), do: GenServer.call(todo_server, {:entries, date})

  def entries(todo_server), do: GenServer.call(todo_server, :entries)

  def add_entries(todo_server, new_entries) do
    GenServer.cast(todo_server, {:add_entries, new_entries})
  end

  def add_entry(todo_server, new_entry) do
    GenServer.cast(todo_server, {:add_entry, new_entry})
  end

  def update_entry(todo_server, entry_id, updater_fun) do
    GenServer.cast(todo_server, {:update_entry, entry_id, updater_fun})
  end

  def update_entry(todo_server, %{} = new_entry) do
    GenServer.cast(todo_server, {:update_entry, new_entry})
  end

  def delete_entry(todo_server, %{id: _} = entry) do
    GenServer.cast(todo_server, {:delete_entry, entry})
  end

  def delete_entry(todo_server, entry_id) do
    GenServer.cast(todo_server, {:delete_entry, entry_id})
  end

  @impl GenServer
  def init({name, entries}), do: {:ok, {name, Todo.Database.get(name) || Todo.List.new(entries)}}

  @impl GenServer
  def handle_call({:entries, date}, _from, {_, todo_list} = state) do
    {:reply, Todo.List.entries(todo_list, date), state}
  end

  @impl GenServer
  def handle_call(:entries, _from, {_, todo_list} = state) do
    {:reply, Todo.List.entries(todo_list), state}
  end

  @impl GenServer
  def handle_cast({:add_entries, new_entries}, {name, todo_list}) do
    new_list = Todo.List.add_entries(todo_list, new_entries)
    Todo.Database.store(name, new_list)
    {:noreply, {name, new_list}}
  end

  @impl GenServer
  def handle_cast({:add_entry, new_entry}, {name, todo_list}) do
    new_list = Todo.List.add_entry(todo_list, new_entry)
    Todo.Database.store(name, new_list)
    {:noreply, {name, new_list}}
  end

  @impl GenServer
  def handle_cast({:update_entry, entry_id, updater_fun}, {name, todo_list}) do
    new_list = Todo.List.update_entry(todo_list, entry_id, updater_fun)
    Todo.Database.store(name, new_list)
    {:noreply, {name, new_list}}
  end

  @impl GenServer
  def handle_cast({:update_entry, %{} = new_entry}, {name, todo_list}) do
    new_list = Todo.List.update_entry(todo_list, new_entry)
    Todo.Database.store(name, new_list)
    {:noreply, {name, new_list}}
  end

  @impl GenServer
  def handle_cast({:delete_entry, %{} = entry}, {name, todo_list}) do
    new_list = Todo.List.delete_entry(todo_list, entry)
    Todo.Database.store(name, new_list)
    {:noreply, {name, new_list}}
  end

  @impl GenServer
  def handle_cast({:delete_entry, entry_id}, {name, todo_list}) do
    new_list = Todo.List.delete_entry(todo_list, entry_id)
    Todo.Database.store(name, new_list)
    {:noreply, {name, new_list}}
  end
end
