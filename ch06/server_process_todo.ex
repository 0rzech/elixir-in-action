defmodule TodoServer do

  def start(), do: ServerProcess.start(TodoServer)

  def add_entries(todo_server, entries) do
    ServerProcess.cast(todo_server, {:add_entries, entries})
  end

  def add_entry(todo_server, entry) do
    ServerProcess.cast(todo_server, {:add_entry, entry})
  end

  def entries(todo_server, date) do
    ServerProcess.call(todo_server, {:entries, date})
  end

  def entries(todo_server) do
    ServerProcess.call(todo_server, :entries)
  end

  def update_entry(todo_server, entry_id, updater_fun) do
    ServerProcess.cast(todo_server, {:update_entry, entry_id, updater_fun})
  end

  def update_entry(todo_server, %{} = new_entry) do
    ServerProcess.cast(todo_server, {:update_entry, new_entry})
  end

  def delete_entry(todo_server, %{id: _} = entry) do
    ServerProcess.cast(todo_server, {:delete_entry, entry})
  end

  def delete_entry(todo_server, entry_id) do
    ServerProcess.cast(todo_server, {:delete_entry, entry_id})
  end

  def init(), do: TodoList.new()

  def handle_call({:entries, date}, state), do: {TodoList.entries(state, date), state}
  def handle_call(:entries, state), do: {TodoList.entries(state), state}
  
  def handle_cast({:add_entries, new_entries}, state), do: TodoList.add_entries(state, new_entries)
  def handle_cast({:add_entry, new_entry}, state), do: TodoList.add_entry(state, new_entry)

  def handle_cast({:update_entry, entry_id, updater_fun}, state) do
    TodoList.update_entry(state, entry_id, updater_fun)
  end

  def handle_cast({:update_entry, %{} = new_entry}, state) do
    TodoList.update_entry(state, new_entry)
  end

  def handle_cast({:delete_entry, %{} = entry}, state), do: TodoList.delete_entry(state, entry)
  def handle_cast({:delete_entry, entry_id}, state), do: TodoList.delete_entry(state, entry_id)
end

defmodule TodoList do

  defstruct auto_id: 1, entries: %{}

  def new(entries \\ []), do: add_entries(%TodoList{}, entries)

  def add_entries(todo_list, entries) do
    Enum.reduce(entries, todo_list, &add_entry(&2, &1))
  end

  def add_entry(todo_list, entry) do
    entry = Map.put(entry, :id, todo_list.auto_id)
    new_entries = Map.put(todo_list.entries, todo_list.auto_id, entry)
    %TodoList{todo_list |
      entries: new_entries,
      auto_id: todo_list.auto_id + 1
    }
  end

  def entries(todo_list, date) do
    todo_list.entries
    |> Stream.filter(fn {_, entry} -> entry.date == date end)
    |> Enum.map(fn {_, entry} -> entry end)
  end

  def entries(todo_list) do
    Map.values(todo_list.entries)
  end

  def update_entry(todo_list, entry_id, updater_fun) do
    case Map.fetch(todo_list.entries, entry_id) do
      :error ->
        todo_list

      {:ok, old_entry} ->
        old_entry_id = old_entry.id
        new_entry = %{id: ^old_entry_id} = updater_fun.(old_entry)
        new_entries = Map.put(todo_list.entries, new_entry.id, new_entry)
        %TodoList{todo_list | entries: new_entries}
    end
  end

  def update_entry(todo_list, %{} = new_entry) do
    update_entry(todo_list, new_entry.id, fn _ -> new_entry end)
  end

  def delete_entry(todo_list, %{id: entry_id} = _entry) do
    delete_entry(todo_list, entry_id)
  end

  def delete_entry(todo_list, entry_id) do
    new_entries = Map.delete(todo_list.entries, entry_id)
    %TodoList{todo_list | entries: new_entries}
  end
end

defmodule TodoList.CsvImporter do

  def import!(file_path) do
    file_path
    |> File.stream!()
    |> Stream.map(&String.replace(&1, "\n", ""))
    |> Stream.map(&parse_entry_fields/1)
    |> Stream.map(&create_entry/1)
    |> TodoList.new()
  end

  defp parse_entry_fields(entry_line) do
    [date_line, title] = String.split(entry_line, ",")

    date = date_line
    |> String.split("/")
    |> Enum.map(&String.to_integer/1)
    |> List.to_tuple()

    {date, title}
  end

  defp create_entry({date, title} = _entry_tuple) do
    {:ok, date} = Date.from_erl(date)
    %{date: date, title: title}
  end
end

defmodule ServerProcess do

  def start(callback_module) do
    spawn(fn ->
      initial_state = callback_module.init()
      loop(callback_module, initial_state)
    end)
  end

  def call(server_pid, request) do
    send(server_pid, {:call, request, self()})

    receive do
      {:response, response} -> response
    end
  end

  def cast(server_pid, request), do: send(server_pid, {:cast, request})

  defp loop(callback_module, current_state) do
    receive do
      {:call, request, caller} ->
        {response, new_state} = callback_module.handle_call(request, current_state)
        send(caller, {:response, response})
        loop(callback_module, new_state)

      {:cast, request} ->
        new_state = callback_module.handle_cast(request, current_state)
        loop(callback_module, new_state)
    end
  end
end
