defmodule SimpleRegistry do
  use GenServer

  def start_link(), do: GenServer.start_link(__MODULE__, nil, name: __MODULE__)

  @impl GenServer
  def init(_) do
    Process.flag(:trap_exit, true)
    :ets.new(__MODULE__, [:named_table, :public, read_concurrency: true, write_concurrency: true])
    {:ok, nil}
  end

  def register(key) do
    Process.link(Process.whereis(__MODULE__))
    case :ets.insert_new(__MODULE__, {key, self()}) do
      true -> :ok
      _    -> :error
    end
  end

  def whereis(key) do
    case :ets.lookup(__MODULE__, key) do
      [{_key, pid}] -> pid
      []            -> nil
    end
  end

  @impl GenServer
  def handle_info({:EXIT, pid, _reason}, state) do
    :ets.match_delete(__MODULE__, {:_, pid})
    {:noreply, state}
  end
end

defmodule SimpleRegistryTest do

  def run() do
    SimpleRegistry.start_link()
    IO.puts("started registry")

    {:ok, pid} = Agent.start_link(fn -> SimpleRegistry.register(:agent) end)
    IO.puts("started agent with pid #{inspect(pid)}")

    pid = SimpleRegistry.whereis(:agent)
    IO.puts("registered agent pid is #{inspect(pid)}")

    pid = SimpleRegistry.whereis(:nothing)
    IO.puts("registered nothing pid is #{inspect(pid)}")

    SimpleRegistry.whereis(:agent) |> Agent.stop()
    IO.puts("stopped registered agent")

    pid = SimpleRegistry.whereis(:agent)
    IO.puts("registered agent pid is #{inspect(pid)}")
  end
end
