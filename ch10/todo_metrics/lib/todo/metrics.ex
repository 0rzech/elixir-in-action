defmodule Todo.Metrics do
  use Task

  def start() do
    Task.start(&execute/0)
  end

  defp execute() do
    IO.inspect([
      memory_usage: :erlang.memory(:total),
      process_count: :erlang.system_info(:process_count)
    ])
  end
end
