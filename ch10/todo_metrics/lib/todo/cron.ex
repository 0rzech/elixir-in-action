defmodule Todo.Cron do
  use Task

  def start_link(_) do
    IO.puts("Starting cron")
    Task.start_link(&loop/0)
  end

  defp loop() do
    Process.sleep(:timer.seconds(10))
    Todo.Metrics.start()
    loop()
  end
end
