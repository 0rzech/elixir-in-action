defmodule Todo.List do

  defstruct auto_id: 1, entries: %{}

  def new(entries \\ []), do: add_entries(%Todo.List{}, entries)

  def add_entries(todo_list, entries) do
    Enum.reduce(entries, todo_list, &add_entry(&2, &1))
  end

  def add_entry(todo_list, entry) do
    entry = Map.put(entry, :id, todo_list.auto_id)
    new_entries = Map.put(todo_list.entries, todo_list.auto_id, entry)
    %Todo.List{todo_list |
      entries: new_entries,
      auto_id: todo_list.auto_id + 1
    }
  end

  def entries(todo_list, date) do
    todo_list.entries
    |> Stream.filter(fn {_, entry} -> entry.date == date end)
    |> Enum.map(fn {_, entry} -> entry end)
  end

  def entries(todo_list) do
    Map.values(todo_list.entries)
  end

  def update_entry(todo_list, entry_id, updater_fun) do
    case Map.fetch(todo_list.entries, entry_id) do
      :error ->
        todo_list

      {:ok, old_entry} ->
        old_entry_id = old_entry.id
        new_entry = %{id: ^old_entry_id} = updater_fun.(old_entry)
        new_entries = Map.put(todo_list.entries, new_entry.id, new_entry)
        %Todo.List{todo_list | entries: new_entries}
    end
  end

  def update_entry(todo_list, %{} = new_entry) do
    update_entry(todo_list, new_entry.id, fn _ -> new_entry end)
  end

  def delete_entry(todo_list, %{id: entry_id} = _entry) do
    delete_entry(todo_list, entry_id)
  end

  def delete_entry(todo_list, entry_id) do
    new_entries = Map.delete(todo_list.entries, entry_id)
    %Todo.List{todo_list | entries: new_entries}
  end
end

defmodule Todo.List.CsvImporter do

  def import!(file_path) do
    file_path
    |> File.stream!()
    |> Stream.map(&String.replace(&1, "\n", ""))
    |> Stream.map(&parse_entry_fields/1)
    |> Stream.map(&create_entry/1)
    |> Todo.List.new()
  end

  defp parse_entry_fields(entry_line) do
    [date_line, title] = String.split(entry_line, ",")

    date = date_line
    |> String.split("/")
    |> Enum.map(&String.to_integer/1)
    |> List.to_tuple()

    {date, title}
  end

  defp create_entry({date, title} = _entry_tuple) do
    {:ok, date} = Date.from_erl(date)
    %{date: date, title: title}
  end
end
