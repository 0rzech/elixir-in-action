defmodule TodoCacheTest do
  use ExUnit.Case

  test "server_process" do
    {:ok, cache} = Todo.Cache.start()
    jessie_pid = Todo.Cache.server_process(cache, "jessie")

    assert jessie_pid != Todo.Cache.server_process(cache, "daria")
    assert jessie_pid == Todo.Cache.server_process(cache, "jessie")
  end

  test "to-do operations" do
    {:ok, cache} = Todo.Cache.start()
    daria = Todo.Cache.server_process(cache, "daria")
    Todo.Server.add_entry(daria, %{date: ~D[2019-01-01], title: "Dentist"})
    entries = Todo.Server.entries(daria, ~D[2019-01-01])

    assert [%{date: ~D[2019-01-01], title: "Dentist"}] = entries
  end
end
